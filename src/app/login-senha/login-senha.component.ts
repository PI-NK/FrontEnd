import { Component, OnInit } from '@angular/core';
import { Directive, forwardRef, Attribute,OnChanges, SimpleChanges,Input } from '@angular/core';
import { NG_VALIDATORS,Validator,Validators,AbstractControl,ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { userDataService } from '../userDataService';
import { User } from '../user'


@Component({
  selector: 'app-login-senha',
  templateUrl: './login-senha.component.html',
  styleUrls: ['./login-senha.component.css']
})

export class LoginSenhaComponent implements OnInit {

  usuario: any;	
  model = new User('','',null,'','','');
  submitted = false;
  message = "";

  constructor(
    private http: Http,
    private router: Router,
    private _userData: userDataService
  ) {
    this.usuario = this._userData.getUserData();
  }

  ngOnInit() {
  }

  submitLogin(): void {
	  let link = 'http://localhost:8080/sessao/checkSenha';
	  this.usuario.senha = this.model.password;
    this.http.post(link, this.usuario, { withCredentials: true })
    .toPromise()
    .then(
      res => {
        this._userData.setUserData(this.usuario);
        this.router.navigate([(<any>res)._body]);
      },
      err => {
        this.message = (<any>err)._body;
    });
  }
}