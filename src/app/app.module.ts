import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EqualTextValidator } from "angular2-text-equality-validator";
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AgendadasComponent } from './agendadas/agendadas.component';
import { AgendamentoComponent } from './agendamento/agendamento.component';
import { ListaEspecialidadesComponent } from './lista-especialidades/lista-especialidades.component';
import { LoginCpfComponent } from './login-cpf/login-cpf.component';
import { LoginSUSComponent } from './login-sus/login-sus.component';
import { AgendahoraComponent } from './agenda-hora/agendahora.component';
import { AgendaConfirmComponent } from './agenda-confirm/agenda-confirm.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginSenhaComponent } from './login-senha/login-senha.component';
import { CpfCnpjModule } from 'ng2-cpf-cnpj';
import { FormsModule } from '@angular/forms';
import { BrMasker4Module } from 'brmasker4';
import { EqualValidator } from './login-senha/password.match.directive';
import { HttpModule } from '@angular/http';
import { TextMaskModule } from 'angular2-text-mask';
import { userDataService } from './userDataService';
import { DynamicModule } from './dynamic-module';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';
import { DemoUtilsModule } from './agendamento/demo-utils/module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AgendadasComponent,
    AgendamentoComponent,
    ListaEspecialidadesComponent,
    LoginCpfComponent,
    LoginSUSComponent,
    AgendahoraComponent,
    AgendaConfirmComponent,
    LoginSenhaComponent,
    EqualValidator
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CpfCnpjModule,
    FormsModule, 
    BrMasker4Module,
    HttpModule,
    TextMaskModule,
    BrowserAnimationsModule, 
    CalendarModule.forRoot(),
    DemoUtilsModule,
    NgbModule.forRoot()
  ],
  providers: [
    userDataService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
