import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { EqualTextValidator } from "angular2-text-equality-validator";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Now!';
  cpf: string;
  cnpj: string;

  ngOnInit() {
    var btnContainer = document.getElementsByClassName("nav");
    var btns = btnContainer[0].getElementsByClassName("btn");
      for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
  
    }
  
  }

  constructor(
    private http: Http
  ) {}  

  logout() {
    let link = 'http://localhost:8080/sessao/logout';
    this.http.post(link, "", { withCredentials: true })
    .toPromise()
    .then(
      res => {
          console.log('')
      },
      err => {
          console.log(err);
    });
  }
}
