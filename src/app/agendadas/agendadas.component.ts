import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-agendadas',
  templateUrl: './agendadas.component.html',
  styleUrls: ['./agendadas.component.css']
})
export class AgendadasComponent implements OnInit {

  consultas = [];

  procedimentos = {
     1: 'saude_idoso',
     2: 'saude_mulher',
     3: 'saude_crianca',
     4: 'odontologia',
     5: 'dermatologia',
  }

  erro = false;

  constructor(
    private http: Http
  ) { }

  ngOnInit() {
  	let link = 'http://localhost:8080/consultas/agendadas';
    this.http.post(link, "", { withCredentials: true })
    .toPromise()
    .then(
      res => {
        let temp = JSON.parse((<any>res)._body);
        for(let i=0; i<temp.length; i++) {
          if(temp[i].proprio == true) {
            let q = new Date(temp[i].horario)
            let p = this.procedimentos[temp[i].procedimento_id].split("_");
            let temp2 = { 
              data: q.toLocaleDateString(),
              hora: q.toLocaleTimeString(),
              procedimento: this.formatProcedimento(p)
            }

            this.consultas.push(temp2);
          }
        }
      },
      err => {
        this.erro = true;
        console.log(err);
    }); 
  }

  formatProcedimento(entry) {
    for(let i=0; i<entry.length; i++ ) {
      entry[i] = entry[i].charAt(0).toUpperCase() + entry[i].slice(1);
    }
    return entry.join(" ");
  }  

}