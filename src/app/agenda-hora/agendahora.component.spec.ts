import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendahoraComponent } from './agendahora.component';

describe('AgendahoraComponent', () => {
  let component: AgendahoraComponent;
  let fixture: ComponentFixture<AgendahoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendahoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendahoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
