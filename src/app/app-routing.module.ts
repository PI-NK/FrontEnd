import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginCpfComponent } from './login-cpf/login-cpf.component';
import { LoginSUSComponent } from './login-sus/login-sus.component';
import { LoginSenhaComponent } from './login-senha/login-senha.component';
import { AgendamentoComponent } from './agendamento/agendamento.component';
import { AgendadasComponent } from './agendadas/agendadas.component';
import { AgendaConfirmComponent } from './agenda-confirm/agenda-confirm.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path:'login', component: LoginComponent },
  { path:'login-cpf', component: LoginCpfComponent },
  { path:'login-sus', component: LoginSUSComponent },
  { path:'login-senha',component: LoginSenhaComponent },
  { path:'agendamento', component: AgendamentoComponent },
  { path:'agendadas' , component: AgendadasComponent },
  { path:'agenda-confirm', component: AgendaConfirmComponent },
  { path: '**', redirectTo: 'login-cpf', pathMatch: 'full' }
];
@NgModule({
  exports: [ RouterModule ],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {}