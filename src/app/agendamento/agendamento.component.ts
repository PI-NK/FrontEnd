import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { 
  CalendarEvent,
  CalendarViewPeriod,
  CalendarMonthViewBeforeRenderEvent,
  CalendarWeekViewBeforeRenderEvent,
  CalendarDayViewBeforeRenderEvent,
  CalendarMonthViewDay
} from 'angular-calendar';

import { colors } from './colors';
import { subDays, addDays } from 'date-fns';

@Component({
  selector: 'app-agendamento',
  templateUrl: './agendamento.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./agendamento.component.css']
})

export class AgendamentoComponent implements OnInit {
  consultas: Array<any> = [];
  procedimentos: Array<any> = [];
  hora = "";
  data = "";
  procedimento = "";
  closeResult: string;
  erro = false;

  constructor(
    private http: Http,    
    private cdr: ChangeDetectorRef,
    private modalService: NgbModal   
  ) {
  }


  ngOnInit() {
    let link = 'http://localhost:8080/procedimentos/todos';
    this.http.post(link, "", { withCredentials: true })
    .toPromise()
    .then(
      res => {
        this.procedimentos = JSON.parse((<any>res)._body);
        for (let i=0; i < this.procedimentos.length; i++) {
          let temp = this.procedimentos[i].procedimento.split("_");
          this.procedimentos[i].procedimento = this.formatProcedimento(temp);
        }
      },
      err => {
          console.log(err);
    });

    link = 'http://localhost:8080/consultas/agendadas';
    this.http.post(link, "", { withCredentials: true })
    .toPromise()
    .then(
      res => {
        this.consultas = JSON.parse((<any>res)._body);
      },
      err => {
        this.erro = true;
          console.log(err);
    });    
  }

  procHidden = true;
  diaHidden = true;

  horariosDia = [];

  diasIndex = {
    1: 'segunda',
    2: 'terca',
    3: 'quarta',
    4: 'quinta',
    5: 'sexta',
  };

  message: string = "";

  view: string = 'month';

  viewDate: Date = new Date();
  viewDateParse = this.viewDate.getTime();
  clickedDate: Date;

  locale: string = 'pt';

  period: CalendarViewPeriod;

  events: CalendarEvent[] = [];

  beforeViewRender(
    { body }: { body: CalendarMonthViewDay[] }
  ): void {
    Array.from(body).forEach(day => {
      console.log()
      console.log()
      if (day.date.getTime() < this.viewDateParse && day.inMonth && this.viewDate.toLocaleDateString() != day.date.toLocaleDateString()) {
        day.cssClass = 'dia-ocupado';
      }
    });
  }

  formatProcedimento(entry) {
    for(let i=0; i<entry.length; i++ ) {
      entry[i] = entry[i].charAt(0).toUpperCase() + entry[i].slice(1);
    }
    return entry.join(" ");
  }

  reverseFormatProcedimento(entry) {
    for(let i=0; i<entry.length; i++ ) {
      entry[i] = entry[i].charAt(0).toLowerCase() + entry[i].slice(1);
    }
    return entry.join("_");
  }

  toggleProcedimentos() {
    this.diaHidden = true;
    this.procHidden = !this.procHidden;
  }

  toggleDia(event?) {
          //console.log(document.getElementsByClassName('horario'))

    if (!event || !event.day.isPast) {
    this.horariosDia = [];
    this.procHidden = true;
    this.diaHidden = !this.diaHidden;

    if (this.diaHidden == false) {
      for (let procedimento in this.procedimentos) {
        if (this.diasIndex[this.clickedDate.getDay()] == this.procedimentos[procedimento]['dia_semana']) {
          this.procedimento = this.procedimentos[procedimento]['procedimento'];
          let temp = this.procedimentos[procedimento]['horario_inicio'].split(":");
          temp[0] = parseInt(temp[0])
          temp[1] = parseInt(temp[1])

          let comp = this.procedimentos[procedimento]['horario_fim'].split(":");
          comp[0] = parseInt(comp[0])
          comp[1] = parseInt(comp[1])

          this.horariosDia.push(temp.join(":") + "0")
          while (temp.join("") != comp.join("")) {
            temp[1] = temp[1] + this.procedimentos[procedimento]['minutos_duracao']
            if (temp[1] == 60) {
              temp[0] += 1;
              temp[1] = 0;
            }

            let joinTemp = temp.join(":");
            if (String(temp[1]).length == 1) {
              joinTemp = joinTemp + "0"
            }
            this.horariosDia.push(joinTemp)
          }
        }
      }
    setTimeout(() => {
      for (let i=0; i<this.consultas.length; i++) {
        let testDate = new Date(this.consultas[i].horario);
        if (testDate.toDateString() == this.clickedDate.toDateString()) {
          document.getElementById(testDate.getHours() + ":" + testDate.getMinutes()).classList.add("ocupado");
        }
    }}, 250)      
    }}

  }

  excludeDays: number[] = [0, 6];

  skipWeekends(direction: 'back' | 'forward'): void {
    if (this.view === 'day') {
      if (direction === 'back') {
        while (this.excludeDays.indexOf(this.viewDate.getDay()) > -1) {
          this.viewDate = subDays(this.viewDate, 1);
        }
      } else if (direction === 'forward') {
        while (this.excludeDays.indexOf(this.viewDate.getDay()) > -1) {
          this.viewDate = addDays(this.viewDate, 1);
        }
      }
    }
  }

  openVerticallyCentered(content, event) {
    this.hora = event.srcElement.id;
    this.data = this.clickedDate.toLocaleDateString();
    this.modalService.open(content, { centered: true });
  }

  confirmarConsulta() {
    let hora = this.hora.split(":");
    let temp = this.clickedDate;
    temp.setHours(parseInt(hora[0]));
    temp.setMinutes(parseInt(hora[1]));
    let proc = this.procedimento.split(" ");
    proc = this.reverseFormatProcedimento(proc)


    let link = 'http://localhost:8080/consultas/agendar';
    this.http.post(link, {
      horario: temp,
      procedimento: proc
    }, { withCredentials: true })
    .toPromise()
    .then(
      res => {
        link = 'http://localhost:8080/consultas/agendadas';
        this.http.post(link, "", { withCredentials: true })
        .toPromise()
        .then(
          res => {
            this.consultas = JSON.parse((<any>res)._body);
            this.toggleDia();
          },
          err => {
            console.log(err);
        });            
      },
      err => {
        console.log(err);
    });    
  }

}