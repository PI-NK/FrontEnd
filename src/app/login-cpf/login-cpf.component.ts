import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { userDataService } from '../userDataService';

import { Usuario } from '../usuario';

@Component({
  selector: 'app-login-cpf',
  templateUrl: './login-cpf.component.html',
  styleUrls: ['./login-cpf.component.css']
})

export class LoginCpfComponent implements OnInit {
  private mask = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public usuario = new Usuario();
  private message = "";

  constructor(
    private http: Http,
    private router: Router,
    private _userData: userDataService
  ) { }

  ngOnInit() { }

  submitLogin(): void {
    if (new RegExp("[0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]-[0-9][0-9]").test(this.usuario.cpf)) {
      var link = 'http://localhost:8080/sessao/checkCpf';
      this.http.post(link, this.usuario)
      .subscribe(
          res => {
            this._userData.setUserData(this.usuario);
            this.router.navigate([(<any>res)._body]);
          },
          err => {
            this.message = (<any>err)._body;
          });   
    } else {
      this.message = "CPF inválido; favor verifique o CPF digitado.";
    }
  }

}