import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCpfComponent } from './login-cpf.component';

describe('LoginCpfComponent', () => {
  let component: LoginCpfComponent;
  let fixture: ComponentFixture<LoginCpfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginCpfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCpfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
