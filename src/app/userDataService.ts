import { Injectable } from '@angular/core';  

@Injectable()
export class userDataService {  
   usuario:any;

   getUserData() { 
      return this.usuario; 
   } 
   
   setUserData(data:any) {
       this.usuario = data;
   }
}