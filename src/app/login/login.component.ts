import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { userDataService } from '../userDataService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  usuario: any;	
  private message = "";
  
  constructor(
    private http: Http,
    private router: Router,
    private _userData: userDataService
  ) { 
	  this.usuario = this._userData.getUserData();
  }

  ngOnInit() {
  }

  submitLogin(): void {
  	let link = 'http://localhost:8080/sessao/login';
  	this.http.post(link, this.usuario, { withCredentials: true } )
  	.toPromise()
  	.then(
      res => {
          this._userData.setUserData(this.usuario);
          this.router.navigate([ JSON.parse((<any>res)._body).url ]);
      },
      err => {
          this.message = (<any>err)._body;
    }); 
  }  

}