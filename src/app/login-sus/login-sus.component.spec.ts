import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSUSComponent } from './login-sus.component';

describe('LoginSUSComponent', () => {
  let component: LoginSUSComponent;
  let fixture: ComponentFixture<LoginSUSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginSUSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSUSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
