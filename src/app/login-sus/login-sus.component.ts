import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { userDataService } from '../userDataService';

@Component({
  selector: 'app-login-sus',
  templateUrl: './login-sus.component.html',
  styleUrls: ['./login-sus.component.css']
})

export class LoginSUSComponent implements OnInit {
    private mask = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]
  usuario: any;
  private message = "";

  constructor(
    private http: Http,
    private router: Router,
    private _userData: userDataService
  ) {
    this.usuario = this._userData.getUserData();
  }

  ngOnInit() {
  }

  submitLogin(): void {
    if (new RegExp("[0-9][0-9][0-9]\.[0-9][0-9][0-9][0-9]\.[0-9][0-9][0-9][0-9]\.[0-9][0-9][0-9][0-9]").test(this.usuario.cardSus)) {
      console.log(this.usuario)
	    let link = 'http://localhost:8080/sessao/checkSus';
      this.http.post(link, this.usuario)
      .toPromise()
      .then(
        res => {
            this._userData.setUserData(this.usuario);    
            this.router.navigate([(<any>res)._body]);
          },
        err => {
            this.message = (<any>err)._body;
        });
    } else {
      this.message = "Cartão SUS inválido; favor verifique o número digitado.";
    }
  }

}