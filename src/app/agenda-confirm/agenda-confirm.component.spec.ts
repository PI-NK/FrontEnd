import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaConfirmComponent } from './agenda-confirm.component';

describe('AgendaConfirmComponent', () => {
  let component: AgendaConfirmComponent;
  let fixture: ComponentFixture<AgendaConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
