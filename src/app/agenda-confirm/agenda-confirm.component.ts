import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-agenda-confirm',
  templateUrl: './agenda-confirm.component.html',
  styleUrls: ['./agenda-confirm.component.css']
})
export class AgendaConfirmComponent implements OnInit {
  closeResult: string;

  constructor(private modalService: NgbModal) {}
  hora = '12:00';
  data = '20/06/2018';
  procedimento = 'Saúde da Mulher';

  ngOnInit() {
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

}
