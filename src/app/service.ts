import { Injectable} from '@angular/core';  

@Injectable()
export class userDataService {  
   logged:any;
   getUserData(){ 
      return this.logged; 
   } 
   setUserData(data:any){
       this.logged = data;
   }
}